import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PresentacionComponent } from './home/presentacion/presentacion.component';
import { UsuarioComponent } from './home/usuario/usuario.component';
import { PerfilComponent } from './home/usuario/perfil/perfil.component';
import { SidebarComponent } from './home/modulos/sidebar/sidebar.component';
import { NavbarComponent } from './home/modulos/navbar/navbar.component';
import {SidebarModule} from "primeng/sidebar";
import {ImageModule} from "primeng/image";
import {SplitButtonModule} from "primeng/splitbutton";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MenuModule} from "primeng/menu";
import {PanelMenuModule} from "primeng/panelmenu";
import {OAuthModule} from "angular-oauth2-oidc";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {ChipModule} from "primeng/chip";
import {GalleriaModule} from "primeng/galleria";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PresentacionComponent,
    UsuarioComponent,
    PerfilComponent,
    SidebarComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SidebarModule,
    ImageModule,
    SplitButtonModule,
    BrowserAnimationsModule,
    MenuModule,
    PanelMenuModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['http://www.angular.at/api'],
        sendAccessToken: true
      }
    }),
    HttpClientModule,
    FormsModule,
    ChipModule,
    GalleriaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
