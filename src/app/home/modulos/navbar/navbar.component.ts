import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DatamoduloService} from "../../../servicios/datamodulo.service";
import {LoginService} from "../../../servicios/login.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(public datamodule: DatamoduloService, public loggin: LoginService) { }

  ngOnInit(): void {
    console.log(this.loggin.getAccessData());
  }

}
