import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-presentacion',
  templateUrl: './presentacion.component.html',
  styleUrls: ['./presentacion.component.css']
})
export class PresentacionComponent implements OnInit {

  images: any[]=[
    {
      previewImageSrc: "assets/image/0.jpg",
      thumbnailImageSrc: "assets/image/0.jpg",
      alt: "Description for Image 1",
      title: "Title 1"
    },
    {
      previewImageSrc: "assets/image/2.jpg",
      thumbnailImageSrc: "assets/image/2.jpg",
      alt: "Description for Image 1",
      title: "Title 1"
    },
    {
      previewImageSrc: "assets/image/3.jpg",
      thumbnailImageSrc: "assets/image/3.jpg",
      alt: "Description for Image 1",
      title: "Title 1"
    }
  ];

  responsiveOptions:any[] = [
    {
      breakpoint: '1024px',
      numVisible: 5,
    },
    {
      breakpoint: '768px',
      numVisible: 3
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
