import { Component } from '@angular/core';
import {AuthConfig, NullValidationHandler,  OAuthService} from "angular-oauth2-oidc";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private  authModuleService:OAuthService) {
    this.configure();
  }
  title = 'agilaSanchez';
  authCodeFlowConfig: AuthConfig = {
    issuer: 'https://lemur-9.cloud-iam.com/auth/realms/agilasanchez',
    redirectUri: window.location.origin+'presentacion',
    clientId: 'AgilaSanchez',
    responseType: 'code',
    scope: 'openid profile email offline_access',
    showDebugInformation: true,
  };
  configure():void{
    this.authModuleService.configure(this.authCodeFlowConfig);
    this.authModuleService.tokenValidationHandler= new NullValidationHandler();
    this.authModuleService.setupAutomaticSilentRefresh();
    this.authModuleService.loadDiscoveryDocument().then(()=>this.authModuleService.tryLogin());
  }
  public islogged():boolean{
    return (this.authModuleService.hasValidIdToken()&&this.authModuleService.hasValidAccessToken())
  }
}
