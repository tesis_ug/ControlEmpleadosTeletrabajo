import { TestBed } from '@angular/core/testing';

import { DatamoduloService } from './datamodulo.service';

describe('DatamoduloService', () => {
  let service: DatamoduloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatamoduloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
