import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {PresentacionComponent} from "./home/presentacion/presentacion.component";
import {PerfilComponent} from "./home/usuario/perfil/perfil.component";
import {UsuarioComponent} from "./home/usuario/usuario.component";

const routes: Routes = [
  {path:'',component: HomeComponent, children:[
      {path:'presentacion', component: PresentacionComponent},
      {path:'usuario', component: UsuarioComponent}
    ]},
  {path:'**',redirectTo:'',pathMatch:'full'}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
